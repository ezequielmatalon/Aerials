package com.luxoft.scheduling;

import java.time.LocalDateTime;
import java.time.Month;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		LocalDateTime a = LocalDateTime.of(2016, Month.SEPTEMBER, 2, 0, 0);
		LocalDateTime b = a.plusDays(7);
		System.out.println(a);
		System.out.println(b);
	}
}
