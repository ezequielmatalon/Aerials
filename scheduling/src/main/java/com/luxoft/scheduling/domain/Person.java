package com.luxoft.scheduling.domain;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class Person {
	private static final int ANDAHALF = 30;
	private static final int OCLOCK = 0;
	private static final String DATE_FORMAT = "dd-M-yyyy hh:mm";
	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);
	private ZoneId zone;
	private Map<LocalDateTime, TypeOfMeeting[][]> meetings = new HashMap<>();

	
	
	public List<String> availableTimesForMeeting(LocalDateTime date, List<Person> persons) {
		List<String> opens = new ArrayList<>();
		Stream<Person> personsStream = persons.stream();
		for (int hour = 0; hour < 24; hour++) {
			final int currentHour = hour;
			if (personsStream.allMatch(p -> p.hasOpenInCalendar(date, currentHour, OCLOCK))) {
				opens.add(formatDate(date, hour, 0));
			}

			if (personsStream.allMatch(p -> p.hasOpenInCalendar(date, currentHour, ANDAHALF))) {
				opens.add(formatDate(date, hour, 30));
			}

		}
		return opens;
	}

	protected Boolean hasOpenInCalendar(LocalDateTime date, Integer hour, Integer minute) {
		if (!meetings.containsKey(date)) {
			return Boolean.TRUE;
		}

		TypeOfMeeting[][] dayMeetings = meetings.get(date);
		TypeOfMeeting meetingAtTime = dayMeetings[hour][minute];

		if (meetingAtTime == null || meetingAtTime.equals(TypeOfMeeting.OPTIONAL)) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	protected Boolean hasOpenForRecurringInCalendar(LocalDateTime startingDate, Integer durationInWeeks, Integer hour,
			Integer minute) {
		for (int i = 0; i <= durationInWeeks; i++) {
			LocalDateTime weekDate = startingDate.plusDays(i * 7);
			if (!hasOpenInCalendar(weekDate, hour, minute)) {
				return Boolean.FALSE;
			}
		}
		return Boolean.TRUE;
	}

	private String formatDate(LocalDateTime date, Integer hour, Integer minute) {
		LocalDateTime exactDate = date.plusHours(hour).plusMinutes(minute);
		ZonedDateTime zonedDateTime = exactDate.atZone(zone);
		return FORMATTER.format(zonedDateTime);
	}

}
