package com.luxoft.scheduling.utils;

import java.time.LocalDateTime;
import java.time.Month;

public class DayUtils {

	private DayUtils() {
	}

	public static LocalDateTime getDay(Integer year, Month month, Integer dayOfMonth) {
		return LocalDateTime.of(year, month, dayOfMonth, 0, 0);
	}

	public static LocalDateTime getDayOfThisMonth(Integer dayOfMonth) {
		LocalDateTime now = LocalDateTime.now();
		return getDayOfThisYear(now.getMonth(), dayOfMonth);
	}

	public static LocalDateTime getDayOfThisYear(Month month, Integer dayOfMonth) {
		LocalDateTime now = LocalDateTime.now();
		return getDay(now.getYear(), month, dayOfMonth);
	}
}
